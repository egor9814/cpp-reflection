#include <iostream>
#include <filesystem>

#ifdef __linux__
#include <dlfcn.h>
#endif //__linux__

#include <reflection/api.hpp>

struct Testing {
    int salt;

    static float variable;

    void method(int v) {
        std::cout << "method(): " << (v + salt) << std::endl;
    }
    void const_method(int v) const {
        std::cout << "const_method(): " << (v + salt) << std::endl;
    }
    void noexcept_method(int v) noexcept {
        std::cout << "noexcept_method(): " << (v + salt) << std::endl;
    }
    void const_noexcept_method(int v) const noexcept {
        std::cout << "const_noexcept_method(): " << (v + salt) << std::endl;
    }

    static void static_method(int v) {
        std::cout << "static_method(): " << v << std::endl;
    }

    static void static_noexcept_method(int v) noexcept {
        std::cout << "static_noexcept_method(): " << v << std::endl;
    }
};
float Testing::variable = 1;

REFLECT_STATIC(Testing)
    REFLECT_METHOD(method)
    REFLECT_METHOD(const_method)
    REFLECT_METHOD(noexcept_method)
    REFLECT_METHOD(const_noexcept_method)
    REFLECT_METHOD(static_method)
    REFLECT_METHOD(static_noexcept_method)
    REFLECT_FIELD(salt)
    REFLECT_FIELD(variable)
REFLECT_END()

struct NoMacroTest {
    int field{0};
    static int sField;

    int method() const {
        return field * 2 + sField;
    }

    explicit NoMacroTest(int init) : field(init) {}
};
inline int NoMacroTest::sField = 0;

int main(int argc, char** argv) {
    std::cout << "Hello, World!" << std::endl;
    Testing instance{3};
    auto cls = reflection::Class::forType<Testing>();
    if (cls) {
        auto m = cls->getMethod("const_method");
        if (m) {
            auto f = cls->getField("salt");
            m->invoke<void>(&instance, 4);
            int old = 0;
            if (f) {
                old = f->get<int>(&instance);
                f->set(&instance, 10);
            }
            m->invoke<void>(&instance, 4);
            if (f) {
                f->set(&instance, old);
            }
            m->invoke<void>(&instance, 4);
        }
        m = cls->getMethod("static_noexcept_method");
        if (m) {
            m->invokeStatic<void>(10);
        }

        auto f = cls->getField("variable");
        if (f) {
            f->set(11.1f);
        }
    }
    std::cout << "static variable: " << Testing::variable << std::endl;
    std::cout << std::endl;

    reflection::api::defineClass<NoMacroTest>()
        .method("m", &NoMacroTest::method)
        .field("f", &NoMacroTest::field)
        .field("s", &NoMacroTest::sField);

    cls = reflection::Class::forType<NoMacroTest>();
    if (cls) {
        auto f = cls->getField("s");
        if (f) {
            f->set(5);
        }

        auto nmtCtor = cls->getConstructor();
        auto nmt = nmtCtor ? nmtCtor->newInstance<NoMacroTest>(-2) : nullptr;
        if (nmt) {
            auto m = cls->getMethod("m");
            if (m) {
                std::cout << "nmt: " << m->invoke<int>(nmt) << std::endl;
            }
            delete nmt;
        }
    }
    std::cout << std::endl;


#ifdef __linux__
    auto path = std::filesystem::path(argv[0]).parent_path();
    path /= "libMyModule.so";
    auto module = dlopen(path.string().c_str(), RTLD_LAZY);
    reflection::external::IExternalModule* m = nullptr;
    if (module) {
        auto proc = dlsym(module, "createModule");
        if (proc) {
            using creator_t = reflection::external::IExternalModule* (*)();
            auto creator = reinterpret_cast<creator_t>(proc);
            if (creator) {
                m = creator();
            }
        }

        if (m) {
            reflection::api(m->getExternalKey());
        }
    }

    auto mathCls = reflection::Class::forName("math");
    if (mathCls) {
        auto pow = mathCls->getMethod("pow");
        if (pow) {
            std::cout << "9 ^ (1/2) = " << pow->invokeStatic<double>(9.0, 0.5) << std::endl;
        }
    }

    if (module) {
        if (m) {
            auto proc = dlsym(module, "destroyModule");
            if (proc) {
                using destroyer_t = void (*)(reflection::external::IExternalModule*);
                auto destroyer = reinterpret_cast<destroyer_t>(proc);
                if (destroyer) {
                    destroyer(m);
                }
            }
        }

        dlclose(module);
    }
#endif
    return 0;
}
