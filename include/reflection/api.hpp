#ifndef __egor9814__cpp_reflection__api_hpp__
#define __egor9814__cpp_reflection__api_hpp__

#include <tuple>
#include <map>
#include <list>
#include <string>
#include <sstream>
#include <utility>

#ifndef __nodiscard__
#define __nodiscard__ [[nodiscard]]
#endif //__nodiscard__


template <typename ClassType>
struct ClassName {
    using type = ClassType;
    static const char* get() {
        return typeid(type).name();
    }
};

#define REFLECTION_RENAME_CLASS(ClassType, NewClassName)\
template <> struct ClassName<ClassType> {\
    using type = ClassType;\
    static const char* get() {\
        return NewClassName;\
    }\
};


namespace reflection {

    namespace __api__ {
        class ReflectedClasses;
        class Reflector;
    }
    class api;
    namespace external {
        class api_key_t {
            friend class reflection::api;
            friend class __api__::ReflectedClasses;
            friend class __api__::Reflector;
            __api__::ReflectedClasses* r;
            static api_key_t* getKey();

            explicit api_key_t(__api__::ReflectedClasses* r) : r(r) {}
            explicit api_key_t(api_key_t*);
        };

        struct IExternalModule {
            virtual api_key_t* getExternalKey() = 0;
        };
    }


    class Class;

    class MethodInfo {
    public:
        struct IInvoker {
            virtual void invoke(void* o, void* a, void* r) = 0;
            virtual void invoke(void* o, void* a) = 0;
            __nodiscard__ virtual bool isStatic() const = 0;
            virtual ~IInvoker() = default;
        };

    private:
        friend class Class;
        friend class __api__::Reflector;

        std::string name;
        IInvoker* invoker;
        bool staticMethod;

        MethodInfo(std::string name, IInvoker *invoker)
                : name(std::move(name)), invoker(invoker) {
            staticMethod = invoker->isStatic();
        }

        ~MethodInfo() {
            delete invoker;
        }

    public:
        __nodiscard__ const std::string& getName() const { return name; }
        __nodiscard__ const bool& isStatic() const { return staticMethod; }

        template <typename ReturnType, typename ClassType, typename ... Args>
        ReturnType invoke(ClassType* instance, Args&&... args) {
            auto o = reinterpret_cast<void*>(instance);
            std::tuple<Args...> arguments(args...);
            auto a = reinterpret_cast<void*>(&arguments);
            if constexpr (std::is_void_v<ReturnType>) {
                invoker->invoke(o, a);
            } else {
                ReturnType result;
                invoker->invoke(o, a, reinterpret_cast<void*>(&result));
                return result;
            }
        }

        template <typename ReturnType, typename ... Args>
        ReturnType invokeStatic(Args&&... args) {
            std::tuple<Args...> arguments(args...);
            auto a = reinterpret_cast<void*>(&arguments);
            if constexpr (std::is_void_v<ReturnType>) {
                invoker->invoke(nullptr, a);
            } else {
                ReturnType result;
                invoker->invoke(nullptr, a, reinterpret_cast<void*>(&result));
                return result;
            }
        }
    };
    using Method = MethodInfo*;
    using MethodId = size_t;
    using MethodTable = std::map<MethodId, Method>;

    class FieldInfo {
    public:
        struct IAccessible {
            virtual void set(void* o, void* v) = 0;
            virtual void get(void* o, void* r) = 0;
            __nodiscard__ virtual bool isStatic() const = 0;
            virtual ~IAccessible() = default;
        };

    private:
        friend class Class;
        friend class __api__::Reflector;

        std::string name;
        IAccessible* access;
        bool staticField;

        FieldInfo(std::string name, IAccessible *access)
                : name(std::move(name)), access(access) {
            staticField = access->isStatic();
        }

        ~FieldInfo() {
            delete access;
        }

    public:
        __nodiscard__ const std::string& getName() const { return name; }
        __nodiscard__ const bool& isStatic() const { return staticField; }

        template <typename Type, typename ClassType>
        Type get(ClassType* instance) {
            auto o = reinterpret_cast<void*>(instance);
            Type result;
            access->get(o, reinterpret_cast<void*>(&result));
            return result;
        }

        template <typename Type>
        Type get() {
            Type result;
            access->get(nullptr, reinterpret_cast<void*>(&result));
            return result;
        }

        template <typename Type, typename ClassType>
        void set(ClassType* instance, const Type& value) {
            auto o = reinterpret_cast<void*>(instance);
            auto v = reinterpret_cast<const void*>(&value);
            access->set(o, const_cast<void*>(v));
        }

        template <typename Type>
        void set(const Type& value) {
            auto v = reinterpret_cast<const void*>(&value);
            access->set(nullptr, const_cast<void*>(v));
        }


        template <typename Type, typename ClassType>
        Type operator()(ClassType* instance) {
            return get<Type, ClassType>(instance);
        }

        template <typename Type>
        Type operator()() {
            return get<Type>();
        }
    };
    using Field = FieldInfo*;
    using FieldId = size_t;
    using FieldTable = std::map<FieldId, Field>;


    class ConstructorInfo {
        friend class Class;
        friend class __api__::Reflector;

        ConstructorInfo() = default;

        ~ConstructorInfo() = default;

    public:
        template <typename ClassType, typename ... Args>
        ClassType* newInstance(Args&&... args) {
            return new ClassType(std::forward<Args>(args)...);
        }
    };
    using Constructor = ConstructorInfo*;


    namespace __detail__ {
        namespace method {

            template <typename FunctionType, typename ReturnType, typename ClassType, typename ... Args>
            struct MethodInvokerBase : MethodInfo::IInvoker {
                using Function = FunctionType;

                FunctionType function;

                explicit MethodInvokerBase(FunctionType function) : function(function) {}

                void invoke(void *o, void *a, void *r) override {
                    auto instance = reinterpret_cast<ClassType*>(o);
                    auto args = reinterpret_cast<std::tuple<Args...>*>(a);
                    auto result = reinterpret_cast<ReturnType*>(r);
                    if constexpr (!std::is_void_v<ReturnType>)
                        *result = call(instance, *args);
                }

                void invoke(void *o, void *a) override {
                    auto instance = reinterpret_cast<ClassType*>(o);
                    auto args = reinterpret_cast<std::tuple<Args...>*>(a);
                    call(instance, *args);
                }

                __nodiscard__ bool isStatic() const override {
                    return false;
                }

            private:
                template<typename Tuple>
                auto call(ClassType* instance, Tuple t) {
                    return __call(instance, function, t);
                }

                template<typename Tuple, size_t ... I>
                static auto __call(ClassType* o, FunctionType f, Tuple t, std::index_sequence<I ...>) {
                    return (o->*f)(std::get<I>(t) ...);
                }
                template<typename Tuple>
                static auto __call(ClassType* o, FunctionType f, Tuple t) {
                    static constexpr auto size = std::tuple_size<Tuple>::value;
                    return __call(o, f, t, std::make_index_sequence<size>{});
                }
            };

            template <typename FunctionType, typename ReturnType, typename ... Args>
            struct StaticMethodInvokerBase : MethodInfo::IInvoker {
                using Function = FunctionType;

                FunctionType function;

                explicit StaticMethodInvokerBase(FunctionType function) : function(function) {}

                void invoke(void *o, void *a, void *r) override {
                    auto args = reinterpret_cast<std::tuple<Args...>*>(a);
                    auto result = reinterpret_cast<ReturnType*>(r);
                    if constexpr (!std::is_void_v<ReturnType>)
                        *result = call(*args);
                }

                void invoke(void *o, void *a) override {
                    auto args = reinterpret_cast<std::tuple<Args...>*>(a);
                    call(*args);
                }

                __nodiscard__ bool isStatic() const override {
                    return true;
                }

            private:
                template<typename Tuple>
                auto call(Tuple t) {
                    return __call(function, t);
                }

                template<typename Tuple, size_t ... I>
                static auto __call(FunctionType f, Tuple t, std::index_sequence<I ...>) {
                    return (f)(std::get<I>(t) ...);
                }
                template<typename Tuple>
                static auto __call(FunctionType f, Tuple t) {
                    static constexpr auto size = std::tuple_size<Tuple>::value;
                    return __call(f, t, std::make_index_sequence<size>{});
                }
            };

#define declare_method_invoker(Name, ...)\
            template <typename ReturnType, typename ClassType, typename ... Args>\
            struct Name##MethodInvoker\
                    : MethodInvokerBase<ReturnType(ClassType::*)(Args...) __VA_ARGS__, ReturnType, ClassType, Args...> {\
                using base = MethodInvokerBase<ReturnType(ClassType::*)(Args...) __VA_ARGS__, ReturnType, ClassType, Args...>;\
                using function_t = typename base::Function;\
                explicit Name##MethodInvoker(function_t f) noexcept : base{f} {}\
            };

            declare_method_invoker(Default)
            declare_method_invoker(Const, const)
            declare_method_invoker(Noexcept, noexcept)
            declare_method_invoker(ConstNoexcept, const noexcept)

#undef declare_method_invoker

#define declare_static_method_invoker(Name, ...)\
            template <typename ReturnType, typename ... Args>\
            struct Static##Name##MethodInvoker\
                    : StaticMethodInvokerBase<ReturnType(*)(Args...) __VA_ARGS__, ReturnType, Args...> {\
                using base = StaticMethodInvokerBase<ReturnType(*)(Args...) __VA_ARGS__, ReturnType, Args...>;\
                using function_t = typename base::Function;\
                explicit Static##Name##MethodInvoker(function_t f) noexcept : base{f} {}\
            };

            declare_static_method_invoker(Default)
            declare_static_method_invoker(Noexcept, noexcept)

#undef declare_static_method_invoker

            template <typename Func>
            struct select_invoker;

#define declare_method_invoker_selector(Type, ...)\
            template <typename ReturnType, typename ClassType, typename ... Args>\
            struct select_invoker<ReturnType(ClassType::*)(Args...) __VA_ARGS__> {\
                using invoker_t = Type <ReturnType, ClassType, Args...>;\
            };

            declare_method_invoker_selector(DefaultMethodInvoker)
            declare_method_invoker_selector(ConstMethodInvoker, const)
            declare_method_invoker_selector(NoexceptMethodInvoker, noexcept)
            declare_method_invoker_selector(ConstNoexceptMethodInvoker, const noexcept)

#undef declare_method_invoker_selector

#define declare_static_method_invoker_selector(Type, ...)\
            template <typename ReturnType, typename ... Args>\
            struct select_invoker<ReturnType(*)(Args...) __VA_ARGS__> {\
                using invoker_t = Type <ReturnType, Args...>;\
            };

            declare_static_method_invoker_selector(StaticDefaultMethodInvoker)
            declare_static_method_invoker_selector(StaticNoexceptMethodInvoker, noexcept)

#undef declare_static_method_invoker_selector

            template <typename Func>
            inline MethodInfo::IInvoker* newInvoker(Func f) noexcept {
                using Invoker = typename select_invoker<Func>::invoker_t;
                return new Invoker(f);
            }

        }

        namespace field {

            template <typename FieldType, typename Type, typename ClassType>
            struct FieldAccessBase : FieldInfo::IAccessible {
                using Field = FieldType;

                Field field;

                explicit FieldAccessBase(Field field) noexcept : field(field) {}

                void set(void *o, void *v) override {
                    auto instance = reinterpret_cast<ClassType*>(o);
                    auto value = reinterpret_cast<Type*>(v);
                    __set(instance, field, *value);
                }

                void get(void *o, void *r) override {
                    auto instance = reinterpret_cast<ClassType*>(o);
                    auto result = reinterpret_cast<Type*>(r);
                    *result = __get(instance, field);
                }

                __nodiscard__ bool isStatic() const override {
                    return false;
                }

            private:
                static void __set(ClassType* o, Field f, const Type& value) {
                    (o->*f) = value;
                }

                static Type __get(ClassType* o, Field f) {
                    return (o->*f);
                }
            };

            template <typename FieldType, typename Type>
            struct StaticFieldAccessBase : FieldInfo::IAccessible {
                using Field = FieldType;

                Field field;

                explicit StaticFieldAccessBase(Field field) noexcept : field(field) {}

                void set(void *o, void *v) override {
                    auto value = reinterpret_cast<Type*>(v);
                    __set(field, *value);
                }

                void get(void *o, void *r) override {
                    auto result = reinterpret_cast<Type*>(r);
                    *result = __get(field);
                }

                __nodiscard__ bool isStatic() const override {
                    return true;
                }

            private:
                static void __set(Field f, const Type& value) {
                    (*f) = value;
                }

                static Type __get(Field f) {
                    return (*f);
                }
            };

            template <typename Type, typename ClassType>
            struct DefaultFieldAccess
                    : FieldAccessBase<Type(ClassType::*), Type, ClassType> {
                using base = FieldAccessBase<Type(ClassType::*), Type, ClassType>;
                using field_t = typename base::Field;
                explicit DefaultFieldAccess(field_t f) noexcept : base{f} {}
            };

            template <typename Type>
            struct StaticFieldAccess
                    : StaticFieldAccessBase<Type(*), Type> {
                using base = StaticFieldAccessBase<Type(*), Type>;
                using field_t = typename base::Field;
                explicit StaticFieldAccess(field_t f) noexcept : base{f} {}
            };

            template <typename F>
            struct select_access;

            template <typename Type, typename ClassType>
            struct select_access<Type(ClassType::*)> {
                using access_t = DefaultFieldAccess<Type, ClassType>;
            };

            template <typename Type>
            struct select_access<Type(*)> {
                using access_t = StaticFieldAccess<Type>;
            };

            template <typename F>
            inline FieldInfo::IAccessible* newAccess(F f) noexcept {
                using Access = typename select_access<F>::access_t;
                return new Access(f);
            }

        }
    }


    class Class {
        friend class __api__::Reflector;
        friend class __api__::ReflectedClasses;

        MethodTable methods;
        FieldTable fields;
        Constructor constructor{new ConstructorInfo};

        std::string name;

        explicit Class(std::string name) : name(std::move(name)) {}

        ~Class() {
            for (auto& it : methods) {
                delete it.second;
            }
            for (auto& it : fields) {
                delete it.second;
            }
            delete constructor;
        }

    public:
        static Class* forName(const std::string& name);

        template <typename ClassType>
        static Class* forType() {
            return forName(ClassName<ClassType>::get());
        }

        __nodiscard__ Method getMethod(const std::string& methodName) const {
            return getMethod(std::hash<std::string>()(methodName));
        }

        __nodiscard__ Method getMethod(MethodId id) const {
            if (methods.count(id))
                return methods.at(id);
            return nullptr;
        }


        __nodiscard__ Field getField(const std::string& fieldName) const {
            return getField(std::hash<std::string>()(fieldName));
        }

        __nodiscard__ Field getField(FieldId id) const {
            if (fields.count(id))
                return fields.at(id);
            return nullptr;
        }


        __nodiscard__ Constructor getConstructor() const {
            return constructor;
        }


        __nodiscard__ const std::string& getName() const { return name; }
    };


    namespace __api__ {

        class ReflectedClasses {
            friend class Reflector;
            friend class reflection::external::api_key_t;
            static ReflectedClasses* instance() {
                static ReflectedClasses rc;
                return &rc;
            }

            std::map<size_t, Class*> map, externals;

            ~ReflectedClasses() {
                for (auto& it : map) {
                    delete it.second;
                }
            }

            Class* find(size_t id) {
                if (map.count(id))
                    return map[id];
                if (externals.count(id))
                    return externals[id];
                return nullptr;
            }
        };

        class Reflector {
            friend class reflection::Class;
            friend class reflection::api;

            Class* cls;
            explicit Reflector(Class* cls) : cls(cls) {}

            template <typename Func>
            void registerMethod(const std::string& methodName, Func f) {
                auto id = std::hash<std::string>()(methodName);
                if (!cls->methods.count(id))
                    cls->methods[id] = new MethodInfo(methodName, __detail__::method::newInvoker(f));
            }

            template <typename Field>
            void registerField(const std::string& fieldName, Field f) {
                auto id = std::hash<std::string>()(fieldName);
                if (!cls->fields.count(id))
                    cls->fields[id] = new FieldInfo(fieldName, __detail__::field::newAccess(f));
            }

            static Reflector* newClass(const std::string& className) {
                auto id = std::hash<std::string>()(className);
                auto rc = ReflectedClasses::instance();
                if (!rc->map.count(id)) {
                    auto cls = new Class(className);
                    rc->map[id] = cls;
                    return new Reflector(cls);
                }
                return nullptr;
            }

            static Class* getClass(const std::string& className) {
                auto id = std::hash<std::string>()(className);
                auto rc = ReflectedClasses::instance();
                return rc ? rc->find(id) : nullptr;
            }
        };

    }

    namespace external {
        api_key_t *api_key_t::getKey() {
            static api_key_t currentApi{__api__::ReflectedClasses::instance()};
            return &currentApi;
        }

        api_key_t::api_key_t(api_key_t *other) : r(getKey()->r) {
            if (r && other && other->r) {
                for (auto& it : other->r->map) {
                    r->externals[it.first] = it.second;
                }
                for (auto& it : other->r->externals) {
                    r->externals[it.first] = it.second;
                }
            }
        }
    }


    class api {
        __api__::Reflector* r;

        explicit api(__api__::Reflector* r) : r(r) {}

    public:
        template <typename ClassType>
        static api defineClass() {
            return api(__api__::Reflector::newClass(ClassName<ClassType>::get()));
        }

        ~api() {
            delete r;
        }

        template <typename Function>
        api& method(const std::string& name, Function f) {
            if (r) r->registerMethod(name, f);
            return *this;
        }

        template <typename Field>
        api& field(const std::string& name, Field f) {
            if (r) r->registerField(name, f);
            return *this;
        }

        static external::api_key_t* getKey() {
            return external::api_key_t::getKey();
        }

        explicit api(external::api_key_t* key) : r(nullptr) {
            external::api_key_t registrar(key);
        }
    };


    Class *Class::forName(const std::string &name) {
        return __api__::Reflector::getClass(name);
    }
}

#define REFLECT(CLASS) { reflection::api::defineClass<CLASS>()
#define REFLECT_STATIC(CLASS) static struct Reflect##CLASS { using class_t = CLASS; Reflect##CLASS(); } Reflect##CLASS##Instance;\
Reflect##CLASS::Reflect##CLASS() REFLECT(CLASS)
#define REFLECT_METHOD(METHOD) .method(#METHOD, &class_t::METHOD)
#define REFLECT_FIELD(FIELD) .field(#FIELD, &class_t::FIELD)
#define REFLECT_END() ; }

#endif //__egor9814__cpp_reflection__api_hpp__
