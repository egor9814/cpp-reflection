#include <reflection/api.hpp>

#include <cmath>

#ifndef API_CALL
#ifdef __linux__
#define API_CALL extern "C" __attribute__ ((visibility("default")))
#endif // __linux__
#endif

struct Math {
    static double pow(double a, double b) {
        return ::pow(a, b);
    }
};

REFLECTION_RENAME_CLASS(Math, "math")
REFLECT_STATIC(Math)
    REFLECT_METHOD(pow)
REFLECT_END()

struct MathModule : reflection::external::IExternalModule {
    reflection::external::api_key_t *getExternalKey() override {
        return reflection::api::getKey();
    }
};


reflection::external::IExternalModule* getInstance() {
    static MathModule module;
    return &module;
}

API_CALL reflection::external::IExternalModule* createModule() {
    return getInstance();
}

API_CALL void destroyModule(reflection::external::IExternalModule*) {}
